<?php
/**
 * @file
 * youtube_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function youtube_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function youtube_views_node_info() {
  $items = array(
    'youtube_video' => array(
      'name' => t('Youtube Video'),
      'base' => 'node_content',
      'description' => t('Add a new youtube video embed code to the website.'),
      'has_title' => '1',
      'title_label' => t('Video Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
